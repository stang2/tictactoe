/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANAWAT_TH
 */
import java.util.*;

import java.util.Scanner;

public class Tictac {

    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static int row, col;
    static char player = 'X';
    static boolean isFinish = false;
    static char winner = '-';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Plese input Row Col:");
           row = kb.nextInt() - 1;
           col = kb.nextInt() - 1;
            System.out.println("row: " + row + " col: " + col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;

            }
            System.out.println("Error");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkDiagonal() {
        for(int col=0;col<3;col++){
            if(table[col][col]!=player){
                return;
            }
        }
        isFinish = true;
        winner = player;
        
    }
    static void checkDiagonal2() {
        int R = 2;
        for(int row=0;row<3;row++,R--){
            if(table[row][R]!=player){
                return;
            }
        }
        isFinish = true;
        winner = player;
        
    }

    static void checkWin() {
      checkRow();
      checkCol();
      checkDiagonal();
      checkDiagonal2();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!!");
        } else {
            showTable();
            System.out.println(winner + " Win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye . . . .");
    }

    public static void main(String[] args) {

        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}